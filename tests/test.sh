#!/bin/sh -e
# Lint
echo "Linting role..."
yamllint ../ || true
# Syntax check
echo "Checking role syntax..."
ansible-galaxy install -r requirements.yml
ansible-playbook --syntax-check all.yml
# Ansible Playbook
echo "Running role..."
ansible-playbook -vv --diff all.yml
echo "Confirming ssh still works..."
ssh-keyscan -t ed25519 centos7
ssh-keyscan -t ed25519 voidlinux
