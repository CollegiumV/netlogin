# netlogin

Configures network login for client endpoints.

## Supported Operating Systems

-   CentOS 7
-   Void Linux

## Requirements

No Requirements

## Role Variables

None

## Role Defaults

    ---
    netlogin_sudo_groups:
      - name: Server Admins
        line: "ALL=(ALL) ALL"  # optional, default is this line

    netlogin_sudo_lecture: true
    netlogin_sudo_lecture_header: bee.lecture
    netlogin_sudo_lecture_contents: |
      Host: {{ inventory_hostname }}

    netlogin_ldap_hosts:
      - ldap://beryllium.collegiumv.org
      - ldap://boron.collegiumv.org

    netlogin_method: nslcd

    netlogin_auth_provider: krb5
    netlogin_auth_realm: COLLEGIUMV.ORG
    netlogin_auth_domain: collegiumv.org
    netlogin_auth_dc:
      - beryllium.collegiumv.org
      - boron.collegiumv.org

    netlogin_mounts:
      - name: homes
        path: /mnt/homes/
        mounts:
          - name: homes
            path: '*'
            fstype: nfs4
            options: 'sec=sys,nolock,noatime,intr'
            server: nfs.collegiumv.org
            remote_path: /srv/homes/&
            pam_mount_path: '~'
            pam_mount_remote: '/srv/homes/%(user)'
            group: Lounge Users
      - name: shares
        path: /mnt/shares/
        mounts:
          - path: ydrive
            fstype: nfs4
            options: 'noblock,noatime,intr'
            server: nfs.collegiumv.org
            remote_path: /srv/ydrive
            group: Lounge Users
          - path: council
            fstype: nfs4
            options: 'noblock,noatime,intr'
            server: nfs.collegiumv.org
            remote_path: /srv/council
            group: Lounge Users
          - path: jumpdrive
            fstype: nfs4
            options: 'noblock,noatime,intr'
            server: nfs.collegium.org
            remote_path: /srv/jumpdrive
            group: Lounge Users


    # Provider specific configuration
    netlogin_pam_mount_logout_wait: 5
    netlogin_ldap_base: 'dc=collegiumv,dc=org'
    netlogin_ldap_sasl: GSS-SPNEGO
    # netlogin_ldap_cert: ...
    netlogin_krb5_log: 'FILE:/var/log/krb5.log'
    netlogin_krb5_database: openldap_ldapconf
    netlogin_pam_mount: true

## Dependencies

-   site-base

## Testing

Tests can be found in the `tests` directory. Current tests validate the following:

-   yamllint
-   Syntax check
-   Provisioning

### Local Testing

Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing

CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

## License

ISC

## Author Information

CV Admins <mailto:cvadmins@utdallas.edu>
